<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="../css/validateuser.css"> 
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 	<link type="text/css" rel="stylesheet" href="../css/materialize2.css"  media="screen,projection"/>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<title>Iniciar2</title>
</head>
<body>
  <div class="content_body">
  <div class="container_body">
<?php
  include("../conex.php");
  $username = $_POST['username'];
  $sql = "SELECT id_login FROM login WHERE username = '$username'";
  $result = mysqli_query($con, $sql);
  $count = mysqli_num_rows($result);
  if($count == 1){
?>

  	<div class="row">
  		<div class="container">
		    <div class="card grey lighten-5 col s6 tarjeta">
		        <div class="card-content white-text">
            <img src="../img/Riberas-Izcaragua-white.png" alt="Riberas-Izcaragua" class="riberas">
              <a href="../index.php" class="card-title brand-logo center white-text text-darken-4">Riberas de Izcaragua</a>
		          <span class="card-title grey-text white-text center">Iniciar sesión</span>
		        </div>
			    <form class="col s12" method="post" action="../login.php">
			      <div class="row">
			        <div class="input-field col s6" style="margin-left: 25%;" hidden>
			          <i class="material-icons prefix">account_circle</i>
			          <input id="username" type="text" name="username" value="<?php echo $username ?>" class="validate" required>
			          <label for="username">Usuario</label>               
			        </div>

              <div class="input-field col s6" style="margin-left: 25%;">
                <i class="material-icons prefix white-text">vpn_key</i>
                <input id="password" type="password" name="password" value="" class="validate password" required>
                <label for="password" class="white-text">Contraseña</label>
              </div>
			      </div>
            <div class="center">                      
              <a class="btn" href="../index.php">Regresar</a>  
              <button class="btn waves-effect waves-light blue" type="submit">Ingresar
              </button>
            </div><br>
            <div class="center">
              <a class="btn waves-effect red" href="../lib/correo/index.php">Olvidó su contraseña?</a>
            </div><br>
			    </form>
		    </div>  			
  		</div>
  	</div>
</body>
<?php
  } else {
    ?>
    <div class="row">
      <div class="container">
        <div class="card blue-grey darken-1 col s12">
            <div class="card-content white-text">
              <span class="card-title center">Usuario invalido</span>
            </div>
            <div class="center">
              <a class="btn waves-effect" href="../index.php">Regresar</a>              
            </div>
            <div class="center">
              <a class="btn waves-effect red" href="../lib/correo/index.php">Olvidó su usuario?</a>
            </div>            
        </div>        
      </div>
    </div>
  
    <?php
  }
?>
 <footer>
    <a class="btn-floating btn-large waves-effect red right btn modal-trigger tooltipped ubication" data-position="left" data-tooltip="Ubicación" href="#modal1"><i class="material-icons">location_on</i></a>
    <div id="modal1" class="modal">
	    <div class="modal-content">
	      <h4 style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Dirección</h4>
	      <p style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Residencias Riberas de Izcaragua, Calle Este, Guarenas 1220, Miranda</p>
	    </div>
	    <div class="modal-footer">
	      <a class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
	    </div>
	  </div>
	  </footer>
 </div>
 <script>
	  $(document).ready(function(){
	    $('.modal').modal();
	  });	
  </script>
  </div>
  </div>
</body>
</html>