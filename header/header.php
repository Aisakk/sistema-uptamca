<?php 
  include('../session.php');
?>  
  <!DOCTYPE html>
  <html>
    <head>
      <link rel="stylesheet" href="../css/header.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize2.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
    </head>
    <style>
      .sidenav{
            overflow-y: hidden !important;
      }
    </style>
    <body>
      <ul id="dropdown1" class="dropdown-content">
        <li><a href="#!" class="">one</a></li>
        <li><a href="#!" class="">two</a></li>
        <li class="divider"></li>
        <li><a href="#!" class="">three</a></li>
      </ul>

      <nav>
        <div class="nav-wrapper menu-nav">
        <img src="../img/Riberas-Izcaragua.png" alt="Riberas-Izcaragua" class="riberas brand-logo right">
          <a href="home.php" class="right text">Riberas de Izcaragua</a>
          <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down ulistico">
          </ul>
        </div>
      </nav>
<?php
  $sql="select * from login where username = '$user_check'";
  $result = mysqli_query($con, $sql);
  $valor = mysqli_fetch_array($result);
?>
      <ul id="slide-out" class="sidenav">
        <li><div class="user-view">
          <div class="background">
            <img src="../img/bg2.png">
          </div>
          <a href="#user"><img class="circle" src="../img/men10.png"></a>
          <a href="#name"><span class="white-text name"><?php echo $valor['nombres'];?></span></a>
          <a href="#email"><span class="white-text email"><?php echo $valor['email'];?></span></a>
        </div></li>
        <li><a href="home.php"><i class="material-icons">home</i>Inicio</a></li>
        
        <?php
          if(isset($_SESSION['login_user'])){
            if($_SESSION['login_user'] == 'admin'){
              echo " ";
            } else {
                echo '<li><a href="factura.php">'.'<i class="material-icons">print</i>'.'Imprimir factura</a></li>';
                echo '<li><a href="deudores.php">'.'<i class="material-icons">check_circle</i>'.'Estadistica de Servicios</a></li>';
                echo '<li><a href="pago.php">'.'<i class="material-icons">swap_horizontal_circle</i>'.'Registrar Pago </a></li>';
              }
          }
        ?>

        <li><div class="divider"></div></li>

        <?php
          if(isset($_SESSION['login_user'])){
            if($_SESSION['login_user'] == 'admin'){
              echo '<li><a href="usuarios.php">'.'<i class="material-icons">people</i>'.'Usuarios</a></li>';
              echo '<li><a href="validar.php">'.'<i class="material-icons">check_circle</i>'.'Validar Pagos</a></li>';
            } else {
                echo " ";
              }
          }
        ?>

        <?php
          if(isset($_SESSION['login_user'])){
            if($_SESSION['login_user'] == 'admin'){
        ?>
              <li><a href="apartamentos.php"><i class="material-icons">domain</i>Apartamentos</a></li>
        <?php
            } else {
                echo " ";
              }
          }
        ?>

        <?php
          if(isset($_SESSION['login_user'])){
            if($_SESSION['login_user'] == 'admin'){
        ?>
              <li><a href="servicios.php"><i class="material-icons">assignment</i>Servicios</a></li>
        <?php
            } else {
                echo " ";
              }
          }
        ?>        

        <li><a class="waves-effect" href="password.php"><i class="material-icons">vpn_key</i>Cambiar Contraseña</a></li>
        <li><a class="waves-effect" href="../logout.php"><i class="material-icons">power_settings_new</i>Salir</a></li>
      </ul>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });     
      </script>
    </body>
  </html>