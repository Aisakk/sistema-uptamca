<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/estilo.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 	<link type="text/css" rel="stylesheet" href="css/materialize2.css"  media="screen,projection"/>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
	<script type="text/javascript" src="js/jquery.min.js"></script>
  	<script type="text/javascript" src="js/materialize.min.js"></script>
	<title>Riberas Izcaragua</title>
</head>
<body>
<div class="content-body col s12">
      <div class="row">
      <div class="container">
		    <div class="card grey lighten-5 col s5 tarjeta">
		        <div class="card-content grey-text darken-2">
				<img src="img/Riberas-Izcaragua-white.png" alt="Riberas-Izcaragua" class="riberas">
                <a href="indexnuevo.php" class="card-title brand-logo center white-text text-darken-4">Riberas de Izcaragua</a>
		          <span class="card-title center white-text">Iniciar sesión</span>
		        </div>
			    <form class="col s12" method="post" action="validate/vuser.php">
			      <div class="row">
			        <div class="input-field col s8" style="margin-left: 10%;">
			          <i class="material-icons prefix white-text">account_circle</i>
			          <input id="username" type="text" name="username" value="" class="validate usuario" required>
			          <label for="username" class="white-text">Usuario</label>
			        </div>
			      </div>

            <div class="center">                      
              <button class="btn waves-effect waves-light blue" type="submit">Validar
              </button>
            </div><br>
			    </form>
		    </div>  			
  		</div>
      </div>
      <footer>
    <a class="btn-floating btn-large waves-effect red right btn modal-trigger tooltipped ubication" data-position="left" data-tooltip="Ubicación" href="#modal1"><i class="material-icons">location_on</i></a>
    <div id="modal1" class="modal">
	    <div class="modal-content">
	      <h4 style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Dirección</h4>
	      <p style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Residencias Riberas de Izcaragua, Calle Este, Guarenas 1220, Miranda</p>
	    </div>
	    <div class="modal-footer">
	      <a class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
	    </div>
	  </div>
	  </footer>
 </div>
 <script>
	  $(document).ready(function(){
	    $('.modal').modal();
	  });	
	</script>
</body>
</html>