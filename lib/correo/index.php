<?php
	$mensaje="Introduzca los Datos";
	if(isset($_POST["envio"])){
    
			include("php/envioCorreo.php");
			$email = new email("Riberas Izcaragua","riberazdeizcaragua@gmail.com","26323268");
			$email->agregar($_POST["email"],$_POST["nombre"]);
						
			if ($email->enviar('Recuperación de cuenta - Riberas de Izcaragua',$contenido_html)){
							
					$mensaje= 'Mensaje enviado a su correo';
							
			}else{
						   
			   $mensaje= 'El mensaje no se pudo enviar';
			   $email->ErrorInfo;
			}
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../../css/materialize.css"  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <title>Iniciar</title>
    <style>
    	nav .nav-wrapper{
    		background-color: white;
    	}
		.brand-logo{
			height: 60px;
			width:60px;
		}
		.container{
			height: auto;
		}
		.row .col.s12{
			height: auto;
			min-height: 300px;
		}
		label{
			color: white;
		}
    </style>
</head>
<body>

      <nav>
       <div class="nav-wrapper menu-nav">
        <img src="../../img/Riberas-Izcaragua.png" alt="Riberas-Izcaragua" class="riberas brand-logo center">
          <a href="home.php" class="right text">Riberas de Izcaragua</a>
          <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      </div>
      </nav>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title">Recuperar Contraseña</span>               
              </div>
            </div>
          </div>
        </div>

    <div class="row">
        <div class="container">
            <div class="card blue-grey darken-1 col s12">
                <div class="card-content white-text">
                  <span class="card-title center">Iniciar sesión</span>
                </div>
                <form class="col s12" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="application/x-www-form-urlencoded" name="envio">
                  <div class="row">
                    <div class="input-field col s6">
                      <i class="material-icons prefix white-text">local_post_office</i>
                      <input id="email" type="email" name="email" value="" class="validate" required>
                      <label class="white-text" for="email">Correo</label>               
                    </div>

                    <div class="input-field col s6">
                      <i class="white-text material-icons prefix">person</i>
                      <input id="nombre" type="text" name="nombre" value="" class="validate" required>
                      <label for="nombre" class="white-text">Nombre</label>               
                    </div>

                    <div class="input-field col s6">
                      <i class="white-text material-icons prefix">fingerprint</i>
                      <input id="cedula" type="number" name="cedula" value="" class="validate" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8">
                      <input name="envio" value="si" hidden="hidden">
                      <label for="cedula" class="white-text">cedula</label>
                    </div>

                  <div class="input-field col s6">
                      <i class="white-text material-icons prefix">phone</i>
                      <input id="telefono" type="number" name="telefono" value="" class="validate" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11">
                      <label for="telefono" class="white-text">Teléfono</label>               
                  </div>                    
                  </div>
                    <div class="center">                      
                      <button class="btn waves-effect waves-light blue" type="submit">Enviar
                      </button>
                    </div><br>
                    <div class="center">
                      <a class="btn" href="../../index.php">Regresar</a>  
                    </div>
                </form>                
            </div>

            <div class="container center">
              <div class="col s12 m12">
                <div class="card deep-orange accent-4">
                  <div class="card-content white-text">
                    <span>
                      <?php
                          echo $mensaje;
                      ?>                      
                    </span>
                  </div>
                </div>
              </div>
            </div>
        </div>        
    </div>    
</body>
<?php
  include('../../footer/footer.php');
?>
</html>