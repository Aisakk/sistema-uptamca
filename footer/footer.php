<meta charset="utf-8">
<footer class="page-footer grey darken-4">
  <div class="footer-copyright">
    <div class="container">
    © 2018 Copyright U.P.T.A.M.C.A
    <a class="btn-floating red right btn modal-trigger tooltipped" data-position="left" data-tooltip="Ubicación" href="#modal1"><i class="material-icons">location_on</i></a>
	  <div id="modal1" class="modal">
	    <div class="modal-content">
	      <h4 style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Dirección</h4>
	      <p style="font-family: sans-serif;
                color: black;
                border-bottom: 1px solid rgb(200, 200, 200);">Residencias Riberas de Izcaragua, Calle Este, Guarenas 1220, Miranda</p>
	    </div>
	    <div class="modal-footer">
	      <a class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
	    </div>
	  </div>
    </div>
  </div>
	<script>
	  $(document).ready(function(){
	    $('.modal').modal();
	  });	
	</script>	  
</footer>