<?php
	ob_start();
	error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
  
	include ('../conex.php');
    include ('../session.php');
//SELECT apartamento, napartamento, habitado, nservicio FROM servicioapartamento INNER JOIN apartamentos ON servicioapartamento.apartamento = apartamentos.id_apartamento
    //$consult = "SELECT * FROM servicioapartamento INNER JOIN apartamentos INNER JOIN servicios ON servicioapartamento.id = apartamentos.id_apartamento";
    //$consult = "SELECT * FROM servicioapartamento INNER JOIN servicios ON servicios.id_servicio WHERE servicioapartamento.apartamento AND servicioapartamento.servicio = servicios.id_servicio AND servicioapartamento.activo = 1 ORDER BY servicioapartamento.apartamento, servicios.id_servicio";
    $consult = "SELECT * FROM servicioapartamento INNER JOIN servicios INNER JOIN apartamentos ON servicios.id_servicio WHERE servicioapartamento.apartamento AND servicioapartamento.servicio = servicios.id_servicio AND servicioapartamento.activo = 1 AND servicioapartamento.apartamento = apartamentos.id_apartamento ORDER BY servicioapartamento.apartamento, servicios.id_servicio";

    $query = mysqli_query($con, $consult);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Deudores y Saldados</title>
</head>
<style>
	.estadistica{
		margin-top: 100px;
	}
	.container-servicios{
		display: flex;
		margin-left: 5%;
	}
	.agua{
		color: rgba(54, 162, 235, 1);
	}
	.gas{
		color:rgba(255, 99, 132, 1);
	}
	.aseo{color: rgba(78, 255, 77, 1);}
	.telefono{color:rgba(0, 0, 0, 1);}
	.electricidad{color:rgba(255, 229, 38, 1);}
	.internet{color:rgba(110, 219, 255, 1);}
	.television{color:rgba(50, 96, 116, 1);}
	.semana{color:rgba(0, 255, 139, 1);}
	.chica{color:rgba(173, 143, 91, 1);}
	.fletes{color:rgba(227, 225, 222, 1);}

</style>
<body>
	 <?php
          include("../header/header.php");
     ?>
     <canvas id="myChart" width="400px" height="200px">
     	
     </canvas>
     <div id="servicios">
     	
     </div>
	<div class="container">
	<?php
	//Saber el Costo Total
	$total = 0;
	//Contador para saber cuantas personas tienen los servicios
	$count_Inter = 0;	
	$count_Telef = 0;
	$count_Agua = 0;
	$count_Elect = 0;
	$count_Gas = 0;
	$count_Aseo = 0;
	$count_Telev = 0; 
	$count_Aseo_Fin = 0;
	$count_Caja_Chi = 0;
	$count_Fletes = 0;    
	echo "<script>
	var total = 0;
	var	totalAgua = 0;
	var totalInternet = 0;
	var totalTelefono = 0;
	var totalElectricidad = 0;
	var totalGas = 0;
	var totalAseo = 0;
	var totalTelevision = 0;
	var totalFinSemana = 0;
	var totalCajaChica = 0;
	var totalFletes = 0;


	var countInter = '$count_Inter',
	 countTelef = '$count_Telef',
	 countAgua = '$count_Agua',
	 countElect = '$count_Elect',
	 countGas = '$count_Gas',
	 countAseo = '$count_Aseo ',
	 countTelev = '$count_Telev',
	 countAseoFin = '$count_Aseo_Fin',
	 countCajaChi = '$count_Caja_Chi',
	 countFletes = '$count_Fletes';
			</script>";
// Aqui termina Contador
			if ($row = mysqli_fetch_array($query)){ 
			   echo "<table border = '1'> \n"; 
			   echo "<tr><td>Piso</td><td>Apartamento</td><td>Servicio</td><td>Costo</td></tr> \n"; 
			   $costo = $row["costo"];
			   echo "<script>
var costoServices = '$costo';

</script>";
			   do { 
			      echo "<tr><td>".$row["piso"]."</td><td>".$row["napartamento"]."</td><td>".$row["nservicio"]."</td><td>".$row["costo"]."</td></tr> \n"; 
			   		
			   		
			      if($row["nservicio"] == "Agua" && $row["costo"]){

			      	echo "<script>
						
							totalAgua = Number(totalAgua)+ Number(costoServices);
							var servicio = 'agua';
							countAgua++;
			      	</script>";
			      }
			       if($row["nservicio"] == "Internet"  && $row["costo"]){
			      	echo "<script>
			   
			      	totalInternet = Number(totalInternet)+ Number(costoServices);
							var servicio2 = 'Internet';
							countInter++;
			      	</script>";
			      }
			      if($row["nservicio"] == "Telefono"  && $row["costo"]){
			      	echo "<script>
			
			      	totalTelefono = Number(totalTelefono)+ Number(costoServices);
							var servicio3 = 'Telefono';
							countTelef++;
					
			      	</script>";
			      }
			      if($row["nservicio"] == "Electricidad"  && $row["costo"]){
			      	echo "<script>
			      	totalElectricidad = Number(totalElectricidad)+ Number(costoServices);
							var servicio4 = 'Electricidad';
							countElect++;
						
			      	</script>";
			      }
			      if($row["nservicio"] == "Gas"  && $row["costo"]){
			      	echo "<script>
			      	totalGas = Number(totalGas)+ Number(costoServices);
							var servicio5 = 'Gas';
							countGas++;
					
			      	</script>";
			      }
			      if($row["nservicio"] == "Aseo"  && $row["costo"]){
			      	echo "<script>
			      	totalAseo = Number(totalAseo)+ Number(costoServices);
							var servicio6 = 'Aseo';
							countAseo++;
						
			      	</script>";
			      }
			      if($row["nservicio"] == "Television"  && $row["costo"]){
			      	echo "<script>
			      	totalTelevision = Number(totalTelevision)+ Number(costoServices);
							var servicio7 = 'Television';
							countTelev++;
						
			      	</script>";
			      }
			      if($row["nservicio"] == "Aseo Fin de Semana"  && $row["costo"]){
			      	echo "<script>
			      	totalFinSemana = Number(totalFinSemana)+ Number(costoServices);
							var servicio8 = 'Aseo Fin de Semana';
							countAseoFin++;
	
			      	</script>";
			      }
			      if($row["nservicio"] == "Caja Chica"  && $row["costo"]){
			      	echo "<script>
			      	totalCajaChica = Number(totalCajaChica)+ Number(costoServices);
							var servicio9 = 'Caja Chica';
							countCajaChi++;

			      	</script>";
			      }
			      if($row["nservicio"] == "Fletes"  && $row["costo"]){
			      	echo "<script>
			      	totalFletes = Number(totalFletes)+ Number(costoServices);
							var servicio6 = 'Fletes';
							countFletes++;
			      	</script>";
			      }
			   } while ($row = mysqli_fetch_array($query)); 
			   echo "</table> \n"; 
} else { 
			echo "¡ No se ha encontrado ningún registro !"; 
} 
		?>

	</div>
	<?php
	include("../footer/footer.php");
	?>
	<script>
		total = totalAgua + totalGas + totalInternet + totalTelefono + totalElectricidad + totalAseo + totalTelevision + totalFinSemana + totalCajaChica + totalFletes;
		let servicios = document.getElementById('servicios');
		servicios.innerHTML = `<div class="container-servicios">
		<p style="margin-right: 5px">Servicios cada Uno: </p> <p class="agua"> Agua ${totalAgua}$ </p>, <p class="gas">Gas ${totalGas}$</p>, 
		<p class="internet">Internet ${totalInternet}$</p>,<p class="telefono"> Telefono ${totalTelefono}$</p>,
		<p class="electricidad">Electricidad ${totalElectricidad}$</p>, <p class="aseo">Aseo ${totalAseo}$</p>,
		<p class="television">Television ${totalTelevision}$</p>, <p class="semana">Aseo Semana ${totalFinSemana}$</p>, <p class="chica">Caja Chica ${totalCajaChica}$</p>,
		<p class="fletes">Fletes ${totalFletes}$</p></div>	
		 `
	</script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Gas', 'Agua', 'Telefono', 'Electricidad', 'Aseo', 'Internet', 'Television', 'Aseo Fin de Semana', 'Caja Chica', 'Fletes'],
        datasets: [{
            label: ['# Personas que tienen el Servicio'],
            data: [countGas, countAgua, countTelef, countElect, countAseo, countInter , countTelev, countAseoFin , countCajaChi, countFletes],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',//gas 
                'rgba(54, 162, 235, 0.2)',//Agua
                'rgba(0, 0, 0, 0.2)',//Telefono
                'rgba(255, 229, 38, 0.2)',//Electricidad
                'rgba(78, 255, 77, 0.2)',//Aseo
                'rgba(110, 219, 255, 0.2)',//Internet
                'rgba(50, 96, 116, 0.2)',//Television
                'rgba(0, 255, 139, 0.2)',//AseoFin de Semana
                'rgba(173, 143, 91, 0.2)',//Caja Chica
                'rgba(227, 225, 222, 0.5)',//Fletes

            ],
            borderColor: [
                'rgba(255, 99, 132, 0.2)',//gas
                'rgba(54, 162, 235, 0.2)',//Agua
                'rgba(0, 0, 0, 0.2)',//Telefono
                'rgba(255, 229, 38, 0.2)',//Electricidad
                'rgba(78, 255, 77, 0.2)',//Aseo
                'rgba(110, 219, 255, 0.2)',//Internet
                'rgba(50, 96, 116, 0.2)',//Television
                'rgba(0, 255, 139, 0.2)',//AseoFin de Semana
                'rgba(173, 143, 91, 0.2)',//Caja Chica
                'rgba(227, 225, 222, 0.5)',//Fletes
            ],
            borderWidth: 2
        },{
        	label: ['Total de todos los Servicios: '+ total+'$']
        }]
    },
  
    options: {
    	 legend: {
		            display: true,
		            position:'top',
		            
		        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
</body>
</html>