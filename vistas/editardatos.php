<!DOCTYPE html>
  <html>
      <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Editar Usuario</title>
          <script>
            function confirmacion(id) {
              if(confirm('¿Esta seguro que desea eliminar el usuario?')){
                location.href='eliminar.php?id='+id
              } else {
                  return false;
                }
            }

            function regresar() {
              location.href='usuarios.php';
            }            
          </script>          
      </head>
      <body>
        <?php
          include("../header/header.php");
        ?>
        <?php
          $id = $_GET['id'];
            $sql = "SELECT * FROM `login` WHERE id_login = '$id'";
            $result = mysqli_query($con, $sql);
        ?>
      
        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title">Editar Usuario</span>
              </div>
            </div>
          </div>
        </div>

        <?php
          while ($valor = mysqli_fetch_array($result)) {
        ?>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="actualizaruser.php" method="post">
                  <div class = "row">

                    <div class = "input-field col s6" hidden>
                      <i class = "material-icons prefix">local_convenience_store</i>
                      <input name="id" id="id" placeholder = "id" type="text" class="active validate" required value="<?php echo $valor['id_login']; ?>"/>
                      <label for = "id">Id</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">fingerprint</i>
                      <input name="cedula" id = "cedula" placeholder = "Cedula" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        maxlength = "8" required value="<?php echo $valor['cedula']; ?>"/>
                      <label for = "cedula">Cedula</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">local_phone</i>
                      <input name="telefono" id = "telefono" placeholder = "Teléfono" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        maxlength = "11" required value="<?php echo $valor['telefono']; ?>"/>
                      <label for = "telefono">Teléfono</label>
                    </div>                    

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">account_circle</i>
                      <input name="nombres" id="nombres" type="text" placeholder="Nombres" class="validate" required value="<?php echo $valor['nombres']; ?>"/>
                      <label for="nombres">Nombre</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">account_circle</i>
                      <input name="apellidos" id = "apellidos" placeholder = "apellidos" type = "text" class = "active validate" required value="<?php echo $valor['apellidos']; ?>"/>
                      <label for = "apellidos">Apellidos</label>
                    </div>
                      
                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">email</i>
                      <input name="email" id="email" placeholder = "Correo" type="text" class="active validate" required value="<?php echo $valor['email']; ?>"/>
                      <label for = "email">Correo</label>
                    </div>
                  </div>

                  <div class="row">                      
                    <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                      <i class="material-icons right">cancel</i>
                    <a>

                    <button class="btn waves-effect waves-light" type="submit">Actualizar
                      <i class="material-icons right">check_circle</i>
                    </button>
                  </div>                  
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php }?>
      </body>
      <?php
        include("../footer/footer.php");
      ?>      
  </html>