<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Actualizar Usuario</title>
    <script>
      function regresar() {
        location.href='usuarios.php'
      }
    </script>      
</head>
<body>
  <?php
    include("../header/header.php");
  ?>

<?php
/*
  var_dump($_POST['id']);
  var_dump($_POST['nombres']);
  var_dump($_POST['apellidos']);
  var_dump($_POST['cedula']);
  var_dump($_POST['telefono']);
  var_dump($_POST['email']);
*/

  	if (empty($_POST['id']) || empty($_POST['nombres']) || empty($_POST['apellidos']) || empty($_POST['cedula'])
		|| empty($_POST['telefono']) || empty($_POST['email'])) {
        $error = "Faltan campos por llenar";
  	} else {
	  	$id = $_POST['id'];
	  	$nombres = $_POST['nombres'];
	  	$apellidos = $_POST['apellidos'];
	  	$cedula = $_POST['cedula'];
	  	$telefono = $_POST['telefono'];
	  	$email = $_POST['email'];

	    $id = stripslashes($id);
	    $nombres = stripslashes($nombres);
	    $apellidos = stripslashes($apellidos);
	    $cedula = stripslashes($cedula);
	    $telefono = stripslashes($telefono);
	    $email = stripslashes($email);

	    //echo $id." ".$nombres." ".$apellidos." ".$cedula." ".$telefono." ".$email;

	    $sql = "UPDATE login SET nombres = '$nombres', apellidos = '$apellidos', cedula = '$cedula', telefono = '$telefono', email = '$email' WHERE id_login = '$id'";

	    $result = mysqli_query($con, $sql);
	    
	    if($result){
	      $error = "Usuario actualizado exitosamente";
	    } else {
	      $error = "Falló en actualizar el usuario";
	    }
      }
?>

    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title"><?php echo $error;?></span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Usuarios</a>            
        </div>
      </div>
    </div>

  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/materialize.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });

    $(document).ready(function(){
      $(".dropdown-trigger").dropdown();
    });
  </script>


</body>
  <?php
    include("../footer/footer.php");
  ?>
</html>