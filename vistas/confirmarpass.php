<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta charset="utf-8">
  <title>Actualizar Contraseña</title>
    <script>
      function regresar() {
        location.href='password.php'
      }      
    </script>      
</head>
<body>
  <?php
    include("../header/header.php");

  	if (empty($_POST['id_login']) || empty($_POST['password'])) {
?>
      <div class="container center">
        <div class="col s12 m6">
          <div class="card blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Faltan campos por llenar</span>
            </div>
            <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">vpn_key</i>Regresar</a>            
          </div>
        </div>
      </div>        
<?php  
  	} else {

  ?>
    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title">Nueva contraseña</span>
          </div>           
        </div>
      </div>
    </div>
<?php      
	  	$id = $_POST['id_login'];
	  	$password = $_POST['password'];

	    $id = stripslashes($id);
	    $password = stripslashes($password);

      $sql = "SELECT password FROM login WHERE id_login = '$id'";


	    //$sql2 = "UPDATE login SET password = '$password' WHERE id_login = '$id'";

	    $result = mysqli_query($con, $sql);
      $asd = mysqli_fetch_array($result);
  	    
        if($password == $asd['password']){
  ?>
          <div class="container center">
            <div class="col s12 m6">
              <div class="card light-blue darken-4">
                <div class="card-content white-text">
                  <form action="actualizarpass.php" method="post">
                    <div class = "row">
                      <div class = "input-field col s6" hidden>
                        <i class = "material-icons prefix">local_convenience_store</i>
                        <input name="id_login" id="id_login" placeholder = "id_login" type="text" class="active validate" required value="<?php echo $valor['id_login']; ?>"/>
                        <label for = "id_login">Id</label>
                      </div>

                      <div class = "input-field col s6 center">
                        <i class = "material-icons prefix">security</i>
                        <input name="password1" id = "password1" placeholder = "Nueva contraseña" class = "active validate"
                          type = "password" required value="" "/>
                        <label for = "password1">Nueva contraseña</label>
                      </div>

                      <div class = "input-field col s6 center">
                        <i class = "material-icons prefix">security</i>
                        <input name="password2" id = "password2" placeholder = "Repita contraseña" class = "active validate"
                          type = "password" required value="" "/>
                        <label for = "password2">Repita contraseña</label>
                      </div>

                      <div class="row">
                        <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                          <i class="material-icons right">cancel</i>
                        </a>

                        <button class="btn waves-effect waves-light" type="submit">Actualizar
                          <i class="material-icons right">check_circle</i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

  <?php
  	    } else {
?>
    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title">Error, la contraseña no coincide</span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">vpn_key</i>Regresar</a>            
        </div>
      </div>
    </div>
<?php          

	        }
      }
?>

  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/materialize.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });

    $(document).ready(function(){
      $(".dropdown-trigger").dropdown();
    });
  </script>


</body>
  <?php
    include("../footer/footer.php");
  ?>
</html>