  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Actualizar Apartamento</title>
        <script>
          function regresar() {
            location.href='disponibles.php'
          }
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");
      ?>

<?php

      if (empty($_POST['id']) || empty($_POST['piso']) || empty($_POST['napartamento'])) {
            $error = "Faltan Campos por llenar";
      } else {
        $piso = $_POST['piso'];
        $napartamento = $_POST['napartamento'];
        $apartamento = $_POST['id'];

        $sql = "UPDATE apartamentos SET piso = '$piso', napartamento = '$napartamento' WHERE id_apartamento = '$apartamento'";
        $resul = mysqli_query($con, $sql);
        if($resul){
          $error = "Se actualizó el apartamento";
        } else {
          $error = "Falló al actualizar";
        }
      }
    ?>
        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $error;?></span>
              </div>
              <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Disponibles</a>            
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>