<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Eliminar Apartamento</title>
    <script>
      function regresar() {
        location.href='disponibles.php'
      }
    </script>      
</head>
<body>
    <?php
      include("../header/header.php");
    ?>

	<?php
	$error='';
		if ($_GET['id']) {
			$_id= $_GET['id'];
			$_id = stripslashes($_id);

				$sql = $con->prepare("DELETE FROM apartamentos WHERE id_apartamento = ?");			
			$sql->bind_param("i",$_id);
			$resultado = $sql->execute();
			if($resultado){
				$error = "Apartamento eliminado exitosamente";
			} else {
				$error = "Error, no se pudo eliminar el apartamento";
			}
		}
	?>

    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title"><?php echo $error;?></span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Disponibles</a>            
        </div>
      </div>
    </div>

  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/materialize.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });

    $(document).ready(function(){
      $(".dropdown-trigger").dropdown();
    });
  </script>


</body>
  <?php
    include("../footer/footer.php");
  ?>
</html>