<?php
	ob_start();
	error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
  
	include ('../conex.php');
    include ('../session.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Validar Pago</title>
</head>
<style>
	.validar_pago{
		min-height: 600px;
		height: auto;
		width: 700px;
		display: flex;
		justify-content: center;
		margin-top: 100px;
		margin-left: 15%;
	}
	.container-table{
		width: 100%;
		height: 100%;
	}
</style>
<body>
	<?php
		include("../header/header.php");
	?>
<div class="validar_pago">
	<div class="container-table">
	<table>
        <thead>
          <tr>
              <th>Usuario</th>
              <th>Monto</th>
              <th>Nro Recibo</th>
              <th>Banco</th>
              <th>Archivo</th>
              <th>Validar</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Adolfo</td>
            <td>20$</td>
            <td>034562372</td>
            <td>Banesco</td>
            <td>abrir</td><!--Este tambien otro boton imagen-->
            <td>validar</td><!--Este campo debe mostrar un boton para validar-->
          </tr>
          
        </tbody>
      </table>
      </div>
</div>
	<?php
		include("../footer/footer.php");
	?>
</body>
</html>