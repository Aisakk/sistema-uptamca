<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Actualizar Servicio</title>
    <script>
      function regresar() {
        location.href='servicios.php'
      }
    </script>      
</head>
<body>
  <?php
    include("../header/header.php");
  ?>

<?php
/*
  var_dump($_POST['id']);
  var_dump($_POST['nombres']);
  var_dump($_POST['apellidos']);
  var_dump($_POST['cedula']);
  var_dump($_POST['telefono']);
  var_dump($_POST['email']);
*/

  	if (empty($_POST['id_servicio']) || empty($_POST['nservicio']) || empty($_POST['costo'])) {
        $error = "Faltan campos por llenar";
  	} else {
	  	$id_servicio = $_POST['id_servicio'];
	  	$nservicio = $_POST['nservicio'];
	  	$costo = $_POST['costo'];

	    $id_servicio = stripslashes($id_servicio);
	    $nservicio = stripslashes($nservicio);
	    $costo = stripslashes($costo);

	    $sql = "UPDATE servicios SET nservicio = '$nservicio', costo = '$costo' WHERE id_servicio = '$id_servicio'";
	    $result = mysqli_query($con, $sql);
	    
	    if($result){
	      $error = "Servicio actualizado exitosamente";
	    } else {
	      $error = "Falló en actualizar el Servicio";
	    }
      }
?>

    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title"><?php echo $error;?></span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">assignment</i>Ver Servicios</a>            
        </div>
      </div>
    </div>

  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/materialize.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });

    $(document).ready(function(){
      $(".dropdown-trigger").dropdown();
    });
  </script>


</body>
  <?php
    include("../footer/footer.php");
  ?>
</html>