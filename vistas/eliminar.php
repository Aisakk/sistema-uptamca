<!DOCTYPE html >
<html>
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="distribution" content="all"/>
    <meta name="revisit" content="1 days"/>
    <title>Eliminar</title>
    <script>
      function regresar() {
        location.href='usuarios.php'
      }
    </script>    
</head>
<body>
	<?php
	  include("../header/header.php");
	?>

		<?php
		$error='';
			if ($_GET['id']) {
				$_id= $_GET['id'];

				$_id = stripslashes($_id);

 				$sql = $con->prepare("DELETE FROM login WHERE apartamento = ?");

 				$sql2 = mysqli_query($con, "UPDATE apartamentos SET habitado = 0 WHERE id_apartamento = '$_id'");

				$sql->bind_param("i",$_id);
				$resultado = $sql->execute();
				if($resultado){
					$error = "Usuario eliminado";
				} else {
					$error = "Error, no se pudo eliminar el usuario";
				}
			}
		?>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $error;?></span>
              </div>
              <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Usuarios</a>            
            </div>
          </div>
        </div>
</body>
      <?php
        include("../footer/footer.php");
      ?>
</html>