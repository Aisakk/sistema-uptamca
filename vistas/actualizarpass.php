<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta charset="utf-8">
  <title>Actualizar contraseña</title>
    <script>
      function regresar() {
        location.href='password.php'
      }
    </script>      
</head>
<body>
  <?php
    include("../header/header.php");
  ?>

<?php

  	if (empty($_POST['id_login']) || empty($_POST['password1']) || empty($_POST['password2'])) {
        $error = "Faltan campos por llenar";
  	} else if ($_POST['password1'] == $_POST['password2']) {
  	  	$id = $_POST['id_login'];
  	  	$password = $_POST['password1'];

  	    $id = stripslashes($id);
  	    $password = stripslashes($password);

  	    $sql = "UPDATE login SET password = '$password' WHERE id_login = '$id'";

  	    $result = mysqli_query($con, $sql);
  	    
  	    if($result){
  	      $error = "Contraseña actualizada exitosamente";
  	    } else {
  	      $error = "Falló en actualizar la contraseña";
  	    }
      } else {
          $error = "Contraseñas no coinciden";
      }
?>

    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title"><?php echo $error;?></span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">vpn_key</i>Regresar</a>            
        </div>
      </div>
    </div>

  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/materialize.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.sidenav').sidenav();
    });

    $(document).ready(function(){
      $(".dropdown-trigger").dropdown();
    });
  </script>


</body>
  <?php
    include("../footer/footer.php");
  ?>
</html>