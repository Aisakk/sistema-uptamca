  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Registro de Usuario</title>
        <script>
          function regresar() {
            location.href='disponibles.php'
          }
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");
      ?>

<?php
    //var_dump($_POST['piso']);

      if (empty($_POST['id']) || empty($_POST['piso']) || empty($_POST['napartamento']) || empty($_POST['username']) || empty($_POST['password']) 
          || empty($_POST['nombres']) || empty($_POST['apellidos']) || empty($_POST['telefono']) || empty($_POST['cedula']) 
          || empty($_POST['email'])) {
            $error = "Faltan Campos por llenar";
      } else {
        $piso = $_POST['piso'];
        $napartamento = $_POST['napartamento'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];
        $telefono = $_POST['telefono'];
        $cedula = $_POST['cedula'];
        $email = $_POST['email'];
        $apartamento = $_POST['id'];

        try {
            // First of all, let's begin a transaction
            $con->begin_transaction();

            // A set of queries; if one fails, an exception should be thrown
            $con->query("INSERT INTO login (username, password, nombres, apellidos, cedula, telefono, email, apartamento) VALUES ('$username', '$password', '$nombres', '$apellidos', '$cedula', '$telefono', '$email', '$apartamento')" );

            $con->query("UPDATE apartamentos SET habitado = 1 WHERE id_apartamento = '$apartamento'");

            // If we arrive here, it means that no exception was thrown
            // i.e. no query has failed, and we can commit the transaction
            $con->commit();
        } catch (Exception $e) {
            echo "excep";
            // An exception has been thrown
            // We must rollback the transaction
            $con->rollback();
        }

        $resul = mysqli_query($con, $sql);
        if($resul){
          $error = "Se Asignó el usuario al apartamento";
        } else {
          $error = "Falló al registrar";
        }
      }
    ?>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $error;?></span>
              </div>
              <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Disponibles</a>            
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>