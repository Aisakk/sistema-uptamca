  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Registro de Servicio</title>
        <script>
          function regresar() {
            location.href='servicios.php'
          }
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");
      ?>

<?php

      if (empty($_POST['nservicio']) || empty($_POST['costo'])) {
            $error = "Faltan Campos por llenar";
      } else {
        $nservicio = $_POST['nservicio'];
        $costo = $_POST['costo'];

        $sqlapa = "SELECT id_apartamento FROM apartamentos";
        $id_apartamento = mysqli_query($con, $sqlapa);
        
        while ($row = @mysqli_fetch_assoc($id_apartamento)) {
          $resultado[] = $row;
        }

        $contador = count($resultado);
       
        $sqlservi = "INSERT INTO servicios (nservicio, costo) VALUES ('$nservicio', '$costo')";
        $resultaddservicio = mysqli_query($con , $sqlservi);
        $ultimoservicio = mysqli_insert_id($con);        

        foreach ($resultado as $key) {
          $id = $key['id_apartamento'];
          $servicioapa = "INSERT INTO servicioapartamento (apartamento, servicio, activo) VALUES ('$id', '$ultimoservicio', false)";
          $resul = mysqli_query($con, $servicioapa);
        }

        if($resul){
          $error = "Se agregó nuevo servicio a los apartamentos";
        } else {
          $error = "Falló al registrar";
        }

      }

    ?>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title"><?php echo $error;?></span>
              </div>
              <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">assignment</i>Ver Servicios</a>            
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>