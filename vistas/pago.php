<?php
	ob_start();
	error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
  
	include ('../conex.php');
    include ('../session.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pago</title>
</head>
<style>
	.container-pago{
		margin-top: 100px;
	
		margin-bottom: 70px;
		display: flex;
		justify-content: center;
	}
	.container-form{
		height: auto;
	}
	.container-form>input{
		width: 30% !important;	
	}
	.container-form>.input{
		margin-left: 20px !important;
	}

	form{
		width: 400px;
		height: 400px;
		background: #636e72;
		display: flex;
		flex-direction: column;
		border-radius: 10px;
		padding: 25px;

	}
	label{
		color: white !important;
	}
	button{
		margin-top: 40px !important;
		width: 100px;
		align-self: center;
		border: none;
		background-color: #2979ff;
		color: white;
		padding: 10px;
		border-radius: 5px;
		cursor: pointer;
	}
	button:hover{
		background-color: #75a7ff;
		transition: 1s ease;
	}
	input[type=text]:focus + label {
     	color: white !important;
	}
	input[type=text]:focus {
     border-bottom: 1px solid white !important;
     box-shadow: 0 1px 0 0 white !important;
   }
   input[type=file]{
   	color: white;
   	border: none;
   	border-radius: 5px;
   	padding: 5px;
   }
</style>
<body>
	<?php
		include("../header/header.php");
	?>
<div class="container-pago">
	<form action="">
		<label for="">Servicio</label>
		<input type="text">
		<label for="">Nro Recibo o Transferencia</label>
		<input type="text">
		<label for="">Foto de transferencia</label><br>
		<input type="file"><br>
		<button>Enviar</button>
	</form>
</div>
	<?php
		include("../footer/footer.php");
	?>
</body>
</html>