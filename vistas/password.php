  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Cambiar Contraseña</title>
    </head>
    <body>

      <?php
        include("../header/header.php");
      ?>

      <div class="container center">
        <div class="col s12 m6">
          <div class="card light-blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Cambiar contraseña</span>
            </div>
          </div>
        </div>
      </div>
      
        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="confirmarpass.php" method="post">
                  <div class = "row">
                    <div class = "input-field col s6" hidden>
                      <i class = "material-icons prefix">local_convenience_store</i>
                      <input name="id_login" id="id_login" placeholder = "id_login" type="text" class="active validate" required value="<?php echo $valor['id_login']; ?>"/>
                      <label for = "id_login">Id</label>
                    </div>

                    <div class = "input-field col s12">
                      <i class = "material-icons prefix">security</i>
                      <input name="password" id = "password" placeholder = "Contraseña Actual"
                        type = "password" class="validate" required value="" "/>
                      <label for = "password">Contraseña Actual</label>
                    </div>                  

                    <div class="row">                      
                      <button class="btn waves-effect waves-light" type="submit">Confirmar
                        <i class="material-icons right">check_circle</i>
                      </button>
                    </div> 
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>