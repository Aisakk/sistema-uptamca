  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Editar Apartamento</title>
        <script>
          function regresar() {
            location.href='disponibles.php'
          }  
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");

        $id = $_GET['id'];

        $sql = "SELECT * FROM `apartamentos` WHERE id_apartamento = $id";
        $result = mysqli_query($con, $sql);

        //var_dump($result['piso']);
        //echo $result['piso'];
        $valor = mysqli_fetch_array($result);
      ?>

      <div class="container center">
        <div class="col s12 m6">
          <div class="card blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Actualizar apartamento</span>
            </div>
          </div>
        </div>
      </div>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="updateapartamento.php" method="post">
                  <div class = "row">
                    <div class = "input-field col s6" style="display:none">
                      <i class = "material-icons prefix">line_weight</i>
                      <input name="id" id = "id" placeholder = "id" class = "validate"
                        type = "text"
                        value = "<?php echo $id; ?>"
                        required />
                      <label for = "id">Id</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">line_weight</i>
                      <input name="piso" id = "piso" placeholder = "piso" class = "validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        value = "<?php echo $valor['piso']; ?>"
                        maxlength = "2" required />
                      <label for = "piso">Piso</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">local_convenience_store</i>
                      <input name="napartamento" id = "napartamento" placeholder = "numero de apartamento" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        value = "<?php echo $valor['napartamento'];?>" 
                        maxlength = "3" required />
                      <label for = "napartamento">Numero de Apartamento</label>
                    </div>
                  </div>

                    <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                      <i class="material-icons right">cancel</i>
                    </a>
                    <button class="btn waves-effect waves-light" type="submit">Actualizar
                      <i class="material-icons right">check_circle</i>
                    </button>
                </form>
              </div>
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>