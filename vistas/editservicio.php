<!DOCTYPE html>
  <html>
      <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Editar Servicio</title>
          <script>
            function regresar() {
              location.href='servicios.php';
            }            
          </script>          
      </head>
      <body>
        <?php
          include("../header/header.php");
        ?>
        <?php
          $id = $_GET['id'];
            $sql = "SELECT * FROM `servicios` WHERE id_servicio = '$id'";
            $result = mysqli_query($con, $sql);
        ?>
      
        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title">Editar Servicio</span>
              </div>
            </div>
          </div>
        </div>

        <?php
          while ($valor = mysqli_fetch_array($result)) {
        ?>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="actualizarservicio.php" method="post">
                  <div class = "row">

                    <div class = "input-field col s6" hidden>
                      <i class = "material-icons prefix">local_convenience_store</i>
                      <input name="id_servicio" id="id_servicio" placeholder = "id" type="text" class="active validate" required value="<?php echo $valor['id_servicio']; ?>"/>
                      <label for = "id">Id</label>
                    </div>                   

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">assignment</i>
                      <input name="nservicio" id="nservicio" type="text" placeholder="Nombres del Servicio" class="validate" required value="<?php echo $valor['nservicio']; ?>"/>
                      <label for="nombres">Nombre de servicio</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">monetization_on</i>
                      <input name="costo" id = "costo" placeholder = "Costo" type = "number" class = "active validate" required value="<?php echo $valor['costo']; ?>"/>
                      <label for = "apellidos">Costo</label>
                    </div>
                  </div>

                  <div class="row">                      
                    <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                      <i class="material-icons right">cancel</i>
                    <a>

                    <button class="btn waves-effect waves-light" type="submit">Actualizar
                      <i class="material-icons right">check_circle</i>
                    </button>
                  </div>                  
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php }?>
      </body>
      <?php
        include("../footer/footer.php");
      ?>      
  </html>