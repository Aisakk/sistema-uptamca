  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Inicio</title>    
    </head>
    <body>

      <?php
        include("../header/header.php");
      ?>
  <div class="contenedor-carousel">
    <div class="carousel carousel-slider" data-indicators="true">
      <a class="carousel-item" href="#one!"><img src="../img/home/1.jpg"></a>
      <a class="carousel-item" href="#two!"><img src="../img/home/2.jpg"></a>
      <a class="carousel-item" href="#three!"><img src="../img/home/3.jpg"></a>
      <a class="carousel-item" href="#four!"><img src="../img/home/4.jpg"></a>
      <a class="carousel-item" href="#five!"><img src="../img/home/5.jpg"></a>
    </div>
  </div>

  <div class="container center">
        <div class="col s12 m6">
          <div class="card light-blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Bienvenido <?php echo $valor['nombres'];?></span>
            </div>
          </div>
        </div>
      </div>

  <br>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
        $('.carousel.carousel-slider').carousel({
    fullWidth: true
  });
       
        $('.carousel').carousel({
            
        });
        autoplay();
        function autoplay() {
            $('.carousel').carousel('next');
            setTimeout(autoplay, 5000);
        }
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>