  <?php
    ob_start();
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
  
    include ('../conex.php');
    include ('../session.php');
    //error_reporting(0);
    $sqlapa = "SELECT apartamento FROM login WHERE username = '$user_check'";
    $apartamento = mysqli_query($con, $sqlapa);
    $idapa = mysqli_fetch_array($apartamento);
    $idapar = $idapa[0];

    $usuario = "SELECT id_servicio, apartamento, nservicio, costo, activo FROM servicioapartamento INNER JOIN servicios ON id_servicio = servicio WHERE apartamento = '$idapar'";

    $usuarios= mysqli_query($con, $usuario);

    $result = mysqli_fetch_array($usuarios);

if(isset($_POST['create_pdf'])){
    ob_start();
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);

    require_once('../lib/tcpdf/tcpdf.php');


    $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Adolfo Mendoza');
    $pdf->SetTitle($_POST['reporte_name']);

    $pdf->setPrintHeader(false); 
    $pdf->setPrintFooter(false);
    $pdf->SetMargins(20, 20, 20, false); 
    $pdf->SetAutoPageBreak(true, 20); 
    $pdf->SetFont('Helvetica', '', 10);
    $pdf->addPage();

    $content = '';
    $total = 0;
    $totalactivo = 0;
    $content .= '
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-align:center;">'.$_POST['reporte_name'].'</h1>

      <table border="1" cellpadding="5">
        <thead>
          <tr>
            <th>Servicio</th>
            <th>Costo</th>
            <th>Estado</th>
          </tr>
        </thead>
    ';

    while ($user= $usuarios->fetch_assoc()) {
      
      $total = $total + $user['costo'] ;
    if($user['activo'] == true){
      $activo = 'Activo';
      $totalactivo = $totalactivo + $user['costo'];
    } else {
      $activo = 'Desactivado';
    }

    $content .= '
        <tr>
            <td>'.$user['nservicio'].'</td>
            <td>'.$user['costo'].'</td>
            <td>'.$activo.'</td>
        </tr>
    ';
    }

    $content .= '</table>  
    <table border="1" cellpadding="5">
    <thead>
      <tr>
        <th>Costo Total de Servicios:</th>
        <td>'.$total.'</td>
        <th>Total a Pagar:'.$totalactivo.'</th>
      </tr>
    </thead>
                </table>';

    $content .= '
        <div class="row padding">
            <div class="col-md-12" style="text-align:center;">
                <span>PDF Generado por </span><a>Riberas Izcaragua</a>
            </div>
        </div>

    ';

    $pdf->writeHTML($content, true, 0, true, 0);

    $pdf->lastPage();
    $pdf->output('Reporte.pdf', 'I');
}

?>
<!DOCTYPE html>
  <html>
      <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
          <script src="../js/jquery.min.js" type="text/javascript" charset="utf-8" async defer></script>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Servicios</title>
          <script>
            
            function regresar() {
              location.href='home.php';
            }

            function enviar() {
              var selected = new Array();
              $(document).ready(function() {
                $("input:checkbox").each(function() {
                    var activo = $(this).attr('activo');
                    var id_servicio = $(this).attr('id_servicio');
                    var id_apartamento = $(this).attr('id_apartamento');

                    //alert(activo+id_apartamento+id_servicio);
                    if (!$(this)[0]['checked'] == '') {
                      //$(this)[0]['checked'] = 'CHECKED';
                      //console.log("verdadero");
                      activo = 1;
                      $(this).toggle(this.checked);
                      //var x = confirm('Desea activar este servicio?');
                      /*setTimeout(() => {
                        window.location= 'addservicio.php?id='+id_apartamento;
                      }, 500);*/
                    } else {
                        //console.log("falso");
                        activo = 0;
                        $(this).toggle(this.checked);
                        //var x = confirm('Desea activar este servicio?');
                        /*
                        setTimeout(() => {
                          window.location= 'addservicio.php?id='+id_apartamento;
                        }, 500);*/
                      }

                        $.ajax({
                            type: 'POST',
                            url: "update.php",
                            data: {
                              activo: activo,
                              id_servicio: id_servicio,
                              id_apartamento: id_apartamento
                            },
                            success: function(data) {
                              if(data == 'correcto'){
                                //window.location= 'apartamentos.php';
                                //alert('Servicios actualizados');
                                //console.log('CORRECTO');
                              }
                            },
                            error: function(xhr) {
                            }
                        });
                });
              });
              return false;
            }
          </script>
      </head>
      <body>
        <?php
          include("../header/header.php");
        ?>
        <?php
            $sql = "SELECT id_servicio, apartamento, nservicio, costo, activo FROM `servicioapartamento` INNER JOIN `servicios` ON id_servicio = servicio WHERE apartamento = '$idapar'";
            $result = mysqli_query($con, $sql);
        ?>
      
        <div class="container center">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title">Servicios</span>
                  <form method="post">
                      <input href="#" type="hidden" name="reporte_name" value="Factura" target="_blank">
                      <input type="submit" name="create_pdf" class="btn btn-danger pull-right" id="PDF"value="Generar PDF" href="#">
                  </form>                
              </div>
            </div>
          </div>
        </div>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <table class="centered highlight">
                  <thead>
                    <tr>
                      <th>Nombre del Servicio</th>
                      <th>Costo</th>
                      <th>Estado</th>
                    </tr>
                  </thead>

                  <tbody>

                  <?php
                    while ($valor = mysqli_fetch_array($result)) {
                  ?>
                    <tr>
                      <td><?php echo $valor['nservicio']; ?></td>
                      <td><?php echo $valor['costo']; ?></td>
                      <?php
                        if($valor['activo'] == 1){
                          echo'
                                <td>
                                  <p>
                                    <label>
                                      <input class="filled-in checkbox-red" activo='.$valor['activo'].' id_servicio='.$valor["id_servicio"].' id_apartamento='.$idapar.' type="checkbox" checked="CHECKED"/>
                                      <span></span>
                                    </label>
                                  </p>
                                </td>
                              ';
                        } else {
                          echo'
                                <td>
                                  <p>
                                    <label>
                                      <input class="filled-in checkbox-red" activo='.$valor['activo'].' id_servicio='.$valor["id_servicio"].' id_apartamento='.$idapar.' type="checkbox"/>
                                      <span></span>
                                    </label>
                                  </p>
                                </td>
                              ';
                        }
                      ?>
                    </tr>
                        
              <?php }?>

                  </tbody>
                </table>
              </div>
              <div class="row">                      
                <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                  <i class="material-icons white-text text-darken-4 right">cancel</i>
                <a>
              </div>
            </div>
          </div>
        </div>
      </body>
      <?php
        include("../footer/footer.php");
      ?>      
  </html>