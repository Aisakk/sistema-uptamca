<?php 
    include ('../conex.php');

    $usuario = 'SELECT * FROM login,apartamentos WHERE login.apartamento = apartamentos.id_apartamento ORDER BY apartamentos.id_apartamento ASC';   
    $usuarios= mysqli_query($con, $usuario);
    $result = mysqli_fetch_array($usuarios);

    //var_dump($result);

if(isset($_POST['create_pdf'])){
    require_once('../lib/tcpdf/tcpdf.php');

    $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Adolfo Mendoza');
    $pdf->SetTitle($_POST['reporte_name']);

    $pdf->setPrintHeader(false); 
    $pdf->setPrintFooter(false);
    $pdf->SetMargins(20, 20, 20, false); 
    $pdf->SetAutoPageBreak(true, 20); 
    $pdf->SetFont('Helvetica', '', 10);
    $pdf->addPage();

    $content = '';

    $content .= '
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-align:center;">'.$_POST['reporte_name'].'</h1>

      <table border="1" cellpadding="5">
        <thead>
          <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Cedula</th>
            <th>Apartamento</th>
          </tr>
        </thead>
    ';

    while ($user= $usuarios->fetch_assoc()) { 
    $content .= '
        <tr>
            <td>'.$user['nombres'].'</td>
            <td>'.$user['apellidos'].'</td>
            <td>'.$user['cedula'].'</td>
            <td>'.$user['apartamento'].'</td>
        </tr>
    ';
    }

    $content .= '</table>';

    $content .= '
        <div class="row padding">
            <div class="col-md-12" style="text-align:center;">
                <span>PDF Generado por </span><a>Riberas Izcaragua</a>
            </div>
        </div>

    ';

    $pdf->writeHTML($content, true, 0, true, 0);

    $pdf->lastPage();
    $pdf->output('Reporte.pdf', 'I');
}

?>

<!DOCTYPE html>
  <html>
      <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link type="text/css" rel="stylesheet" href="../css/materialize2.css"  media="screen,projection"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Usuarios</title>
          <script>
            function confirmacion(id) {
              if(confirm('¿Esta seguro que desea eliminar el usuario?')){
                location.href='eliminar.php?id='+id
              } else {
                  return false;
                }
            }

            function editar(id) {
              location.href='editardatos.php?id='+id
            }             
          </script>
      </head>
      <body>

        <?php if (isset($_GET['bootstrap']) && $_GET['bootstrap'] == 3): ?>
        <?php elseif (isset($_GET['bootstrap']) && $_GET['bootstrap'] == 4): ?>
        <?php else: ?>
        <link rel="stylesheet" href="../lib/zebra_pagination.css" type="text/css">
        <?php endif; ?>

        <?php
          include("../header/header.php");        
        ?>
 
        <div class="container center propietarios">
          <div class="col s12 m6">
            <div class="card blue darken-3">
              <div class="card-content white-text">
                <span class="card-title">Lista de Propietarios</span>
                <div class="col-md-12">
                  <form method="post">
                      <input type="hidden" name="reporte_name" value="Usuarios con acceso al sistema">
                      <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="Generar PDF" target="_blank">
                  </form>
                </div>               
              </div>
            </div>
          </div>
        </div>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <table class="centered highlight">
                  <thead>
                    <tr>
                      <th>Nombres</th>
                      <th>Apellidos</th>
                      <th>C.I.</th>
                      <th>Apartamento</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>

                  <?php
                    $records_per_page = 6;
                    require '../lib/Zebra_Pagination.php';
                    $pagination = new Zebra_Pagination();

                    $sql = 'SELECT * FROM login,apartamentos WHERE login.apartamento = apartamentos.id_apartamento ORDER BY apartamentos.id_apartamento ASC';

                    $result = @mysqli_query($con, $sql);
                    // fetch the total number of records in the table
                    #$rows = @pg_fetch_assoc($result) or die('failed');                
                    while ($row = @mysqli_fetch_assoc($result)) {
                      $resultado[] = $row;
                      //var_dump($row);
                    }
                    // pass the total number of records to the pagination class
                    $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

                    $pagination->records(count($resultado));
                    // records per page
                    $pagination->records_per_page($records_per_page);
                    
                    $resultado = array_slice(
                        $resultado,
                        (($pagination->get_page() - 1) * $records_per_page),
                        $records_per_page
                    );
                    //var_dump($resultado);
                  ?>


                  <tbody>
                  <?php
                    $index = 0; foreach ($resultado as $row) {
                  ?>
                    <tr>
                      <td><?php echo $row['nombres']; ?></td>
                      <td><?php echo $row['apellidos']; ?></td>
                      <td><?php echo $row['cedula']; ?></td>
                      <td><?php echo $row['napartamento']; ?></td>
                      <td>
                        <button class="btn light-blue tooltipped" data-tooltip="Editar" value="<?php echo $row['id_login'];?>" onClick="editar(this.value)"><i class="material-icons white-text text-darken-4">edit</i></button>

                        <button class="btn red tooltipped" data-tooltip="Eliminar" value="<?php echo $row['apartamento'];?>" onClick="confirmacion(this.value)"><i class="material-icons white-text text-darken-4">delete</i></button>
                      </td>
                    </tr>
                        
              <?php }?>

                  </tbody>
                </table>
                <?php
                $pagination->render();
                ?>                
              </div>
            </div>
          </div>
        </div>
        <script>
          $(document).ready(function(){
            $('.tooltipped').tooltip();
          });          
        </script>
      </body>
      <?php
        include("../footer/footer.php");
      ?>      
  </html>