<!DOCTYPE html>
<html>
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Nuevo Apartamento</title>
      <script>
        function regresar() {
          location.href='disponibles.php'
        }

        function enviar() {
          var selected = new Array();
          $(document).ready(function() {
            $("input:checkbox").each(function() {
                var activo = $(this).attr('activo');
                var id_servicio = $(this).attr('id_servicio');
                var id_apartamento = $(this).attr('id_apartamento');

                //alert(activo+id_apartamento+id_servicio);
                if (!$(this)[0]['checked'] == '') {
                  //$(this)[0]['checked'] = 'CHECKED';
                  //console.log("verdadero");
                  activo = 1;
                  $(this).toggle(this.checked);
                  //var x = confirm('Desea activar este servicio?');
                  /*setTimeout(() => {
                    window.location= 'addservicio.php?id='+id_apartamento;
                  }, 500);*/
                } else {
                    //console.log("falso");
                    activo = 0;
                    $(this).toggle(this.checked);
                    //var x = confirm('Desea activar este servicio?');
                    /*
                    setTimeout(() => {
                      window.location= 'addservicio.php?id='+id_apartamento;
                    }, 500);*/
                  }

                    $.ajax({
                        type: 'POST',
                        url: "addservicioap.php",
                        data: {
                          activo: activo,
                          id_servicio: id_servicio,
                          id_apartamento: id_apartamento
                        },
                        success: function(data) {
                          if(data == 'correcto'){
                            //window.location= 'apartamentos.php';
                            //alert('Servicios actualizados');
                            //console.log('CORRECTO');
                          }
                        },
                        error: function(xhr) {
                        }
                    });
            });
          });
          return false;
        }
      </script>      
  </head>
  <body>
    <?php
      include("../header/header.php");
      //var_dump($_POST['piso']);

    if (empty($_POST['piso']) || empty($_POST['napartamento'])) {
          $error = "Faltan Campos por llenar";
    } else {
      $piso = $_POST['piso'];
      $napartamento = $_POST['napartamento'];

      $piso = stripslashes($piso);
      $napartamento = stripslashes($napartamento);

      $sql = "INSERT INTO apartamentos (piso, napartamento) VALUES ('$piso', '$napartamento')";

      $resul = mysqli_query($con, $sql);

      if($resul){
        $error = "Apartamento Registrado";

        $sql = "SELECT * FROM `servicios`";

        $id = "SELECT id_apartamento FROM apartamentos WHERE piso = '$piso' AND napartamento = '$napartamento'";

        $id_result = mysqli_query($con, $id);

        $id_apartamento = mysqli_fetch_array($id_result);

        $apartamento = $id_apartamento['id_apartamento'];

        $result2 = mysqli_query($con, $sql);

        while ($valor = mysqli_fetch_array($result2)) {
          $apartamento = $apartamento;
          $id_servicio = $valor["id_servicio"];
          //echo $apartamento;
          $activo = 0;

          $servicios = "INSERT INTO servicioapartamento (apartamento, servicio, activo) VALUES ('$apartamento', '$id_servicio', '$activo')";

          $addservicios = mysqli_query($con, $servicios);
        }
      } else {
        $error = "Falló al registrar apartamento";         
      }
    }
  ?>

    <div class="container center">
      <div class="col s12 m6">
        <div class="card blue darken-3">
          <div class="card-content white-text">
            <span class="card-title"><?php echo $error;?></span>
          </div>
          <a class="waves-effect waves-light btn" onclick="regresar();"><i class="material-icons right">domain</i>Ver Disponibles</a>            
        </div>
      </div>
    </div>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/materialize.min.js"></script>

    <script>
      $(document).ready(function(){
        $('.sidenav').sidenav();
      });

      $(document).ready(function(){
        $(".dropdown-trigger").dropdown();
      });
    </script>


  </body>
    <?php
      include("../footer/footer.php");
    ?>
</html>