  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Registrar Dueño</title>
        <script>
          function regresar() {
            location.href='disponibles.php'
          }  
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");

        $id = $_GET['id'];

        $sql = "SELECT * FROM `apartamentos` WHERE id_apartamento = $id";
        $result = mysqli_query($con, $sql);

        //var_dump($result['piso']);
        //echo $result['piso'];
        $valor = mysqli_fetch_array($result);
      ?>

      <div class="container center">
        <div class="col s12 m6">
          <div class="card blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Registrar Dueño de apartamento</span>
            </div>
          </div>
        </div>
      </div>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="registraruser.php" method="post">
                  <div class = "row">

                    <div class = "input-field col s6" style="display:none">
                      <i class = "material-icons prefix">line_weight</i>
                      <input name="id" id = "id" placeholder = "id" class = "validate"
                        type = "text"
                        value = "<?php echo $id; ?>"
                        required />
                      <label for = "id">Id</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">line_weight</i>
                      <input name="piso" id = "piso" placeholder = "piso" class = "validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        value = "<?php echo $valor['piso']; ?>"
                        maxlength = "2" required />
                      <label for = "piso">Piso</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">local_convenience_store</i>
                      <input name="napartamento" id = "napartamento" placeholder = "numero de apartamento" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        value = "<?php echo $valor['napartamento'];?>" 
                        maxlength = "2" required />
                      <label for = "napartamento">Numero de Apartamento</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">account_circle</i>
                      <input name="username" id="username" placeholder = "Nombre de Usuario" type="text" class="active validate" required/>
                      <label for = "username">Nombre de Usuario</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">security</i>
                      <input name="password" id="password" placeholder = "Contraseña" type="password" class="active validate" required/>
                      <label for = "password">Contraseña</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">account_circle</i>
                      <input name="nombres" id = "nombres" placeholder = "Nombres" class = "validate"
                        type = "text" required />
                      <label for = "nombres">Nombres</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">account_circle</i>
                      <input name="apellidos" id = "apellidos" placeholder = "Apellidos" class = "active validate"
                        type = "text" required />
                      <label for = "apellidos">Apellidos</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">local_phone</i>
                      <input name="telefono" id = "telefono" placeholder = "Teléfono" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        maxlength = "11" required/>
                      <label for = "telefono">Teléfono</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">fingerprint</i>
                      <input name="cedula" id = "cedula" placeholder = "Cedula" class = "active validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        type = "number"
                        maxlength = "8" required/>
                      <label for = "cedula">Cedula</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">email</i>
                      <input name="email" id="email" placeholder = "Correo" type="email" class="active validate" required/>
                      <label for = "email">Correo</label>
                    </div>
                  </div>

                    <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                      <i class="material-icons right">cancel</i>
                    </a>
                    <button class="btn waves-effect waves-light" type="submit">Registrar
                      <i class="material-icons right">check_circle</i>
                    </button>

                </form>
              </div>
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>