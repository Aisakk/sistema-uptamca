  <!DOCTYPE html>
  <html>
    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Nuevo Servicio</title>
        <script>
          function regresar() {
            location.href='servicios.php'
          }  
        </script>      
    </head>
    <body>
      <?php
        include("../header/header.php");

        //$sql = "SELECT * FROM `apartamentos` WHERE id_apartamento = $id";
        //$result = mysqli_query($con, $sql);

        //var_dump($result['piso']);
        //echo $result['piso'];
        //$valor = mysqli_fetch_array($result);
      ?>

      <div class="container center">
        <div class="col s12 m6">
          <div class="card blue darken-3">
            <div class="card-content white-text">
              <span class="card-title">Agregar nuevo Servicio</span>
            </div>
          </div>
        </div>
      </div>

        <div class="container center">
          <div class="col s12 m6">
            <div class="card light-blue darken-4">
              <div class="card-content white-text">
                <form action="registrarservicio.php" method="post">
                  <div class = "row">

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">assignment</i>
                      <input name="nservicio" id="nservicio" placeholder = "Nombre del Servicio" value="" type="text" class="active validate" required/>
                      <label for = "nservicio">Nombre del Servicio</label>
                    </div>

                    <div class = "input-field col s6">
                      <i class = "material-icons prefix">monetization_on</i>
                      <input name="costo" id = "costo" placeholder = "Costo" value="" class = "active validate"
                        type = "number" required/>
                      <label for = "costo">Costo</label>
                    </div>

                  </div>

                    <a class="btn waves-effect red" name="action" onclick="regresar()">Cancelar
                      <i class="material-icons right">cancel</i>
                    </a>
                    <button class="btn waves-effect waves-light" type="submit">Registrar
                      <i class="material-icons right">check_circle</i>
                    </button>

                </form>
              </div>
            </div>
          </div>
        </div>

      <script type="text/javascript" src="../js/jquery.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>

      <script>
        $(document).ready(function(){
          $('.sidenav').sidenav();
        });

        $(document).ready(function(){
          $(".dropdown-trigger").dropdown();
        });
      </script>


    </body>
      <?php
        include("../footer/footer.php");
      ?>
  </html>